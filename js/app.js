(function() {
    'use strict';

    angular.module('weather-app', []);


    angular
        .module('weather-app')
        .controller('WeatherController', WeatherController);

    WeatherController.$inject = ['weatherService'];
    function WeatherController(weatherService) {
        var vm = this;

        activate();

        ////////////////

        function activate() { 
            weatherService.getWeatherService().then(function (result) {
                angular.extend(vm, result);
            });
        }
    }


    angular
        .module('weather-app')
        .factory('weatherService', weatherService);

    weatherService.$inject = ['$q'];
    function weatherService($q) {
        var service = {
            getWeatherService: getWeatherService
        };
        
        return service;

        ////////////////
        
        /*
         * Get Weather Info Mock Service
         */
        function getWeatherService() {
            return $q.when({
                temperature : 26,
                status : 'Clear',
                place : 'London, GB',
                todayForcasts : [
                    {time: '12PM', temp: '29', icon:'wi-day-sunny'},
                    {time: '1PM', temp: '28', icon:'wi-day-cloudy'},
                    {time: '2PM', temp: '27', icon:'wi-day-cloudy'},
                    {time: '3PM', temp: '29', icon:'wi-day-cloudy'},
                    {time: '4PM', temp: '28', icon:'wi-day-sunny'},
                    {time: '5PM', temp: '29', icon:'wi-day-sunny'},
                    {time: '6PM', temp: '30', icon:'wi-day-sunny'},
                    {time: '7PM', temp: '29', icon:'wi-day-cloudy'},
                ],
                futureDaysForcasts : [
                    { icon: 'wi-day-cloudy', day:'Sat', minTemp: 29, maxTemp: 30},
                    { icon: 'wi-day-cloudy', day:'Sun', minTemp: 29, maxTemp: 30},
                    { icon: 'wi-day-cloudy', day:'Mon', minTemp: 29, maxTemp: 30},
                    { icon: 'wi-day-cloudy', day:'Tue', minTemp: 29, maxTemp: 30},
                ]
            });
         }
    }
    
})();